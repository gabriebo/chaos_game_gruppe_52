package edu.ntnu.stud.chaosgame.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ComplexTest {

    @Test
    void testGetReal() {
        Complex complex = new Complex(3.0, 4.0);

        double realPart = complex.getReal();

        assertEquals(realPart, 3.0);
    }

    @Test
    void testGetImaginary() {
        Complex complex = new Complex(3.0, 4.0);

        double imaginaryPart = complex.getImaginary();

        assertEquals(imaginaryPart, 4.0);
    }

    @Test
    void testAdd() {
        Complex complex1 = new Complex(3.0, 4.0);
        Complex complex2 = new Complex(1.0, 2.0);

        Complex result = complex1.add(complex2);

        assertEquals(result.getReal(), 4.0);
        assertEquals(result.getImaginary(), 6.0);
    }

    @Test
    void testSubtract() {
        Complex complex1 = new Complex(3.0, 4.0);
        Complex complex2 = new Complex(1.0, 2.0);

        Complex result = complex1.subtract(complex2);

        assertEquals(result.getReal(), 2.0);
        assertEquals(result.getImaginary(), 2.0);
    }

    @Test
    void testNegate() {
        Complex complex = new Complex(3.0, 4.0);

        Complex result = complex.negate();

        assertEquals(result.getReal(), -3.0);
        assertEquals(result.getImaginary(), -4.0);
    }

    @Test
    void testSqrt() {
        Complex complex = new Complex(3.0, 4.0);

        Complex result = complex.sqrt();

        assertEquals(result.getReal(), 2.0, 0.00001);
        assertEquals(result.getImaginary(), 1.0, 0.00001);
    }
}