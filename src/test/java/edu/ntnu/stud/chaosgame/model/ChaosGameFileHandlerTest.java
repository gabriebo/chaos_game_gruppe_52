package edu.ntnu.stud.chaosgame.model;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class ChaosGameFileHandlerTest {
  @Test
    void testReadChaosGameDescriptionAffine() throws IOException {
        ChaosGameFileHandler handler = new ChaosGameFileHandler();
        ChaosGameDescription chaosGameDescription = handler.readFromFile("src/test/resources/sierpinski.txt");
        assertEquals(chaosGameDescription.getMinCoords().getx0(), 0.0);
        assertEquals(chaosGameDescription.getMinCoords().getx1(), 0.0);
        assertEquals(chaosGameDescription.getMaxCoords().getx0(), 1.0);
        assertEquals(chaosGameDescription.getMaxCoords().getx1(), 1.0);
        assertEquals(chaosGameDescription.getTransforms().size(), 3);
    }

@Test
    void testReadChaosGameDescriptionJulia() throws IOException {
        ChaosGameFileHandler handler = new ChaosGameFileHandler();
        ChaosGameDescription chaosGameDescription = handler.readFromFile("src/test/resources/julia.txt");
        assertEquals(chaosGameDescription.getMinCoords().getx0(), -1.6);
        assertEquals(chaosGameDescription.getMinCoords().getx1(), -1.0);
        assertEquals(chaosGameDescription.getMaxCoords().getx0(), 1.6);
        assertEquals(chaosGameDescription.getMaxCoords().getx1(), 1.0);
        assertEquals(chaosGameDescription.getTransforms().size(), 2);
    }

    @Test
    void testWriteChaosGameDescriptionAffine() throws IOException {
        ChaosGameFileHandler handler = new ChaosGameFileHandler();
        ChaosGameDescription chaosGameDescription = handler.readFromFile("src/test/resources/sierpinski.txt");
        handler.writeToFile("src/test/resources/sierpinski_out.txt", chaosGameDescription);
        ChaosGameDescription chaosGameDescriptionOut = handler.readFromFile("src/test/resources/sierpinski_out.txt");
        assertEquals(chaosGameDescription.getMinCoords().getx0(), chaosGameDescriptionOut.getMinCoords().getx0());
        assertEquals(chaosGameDescription.getMinCoords().getx1(), chaosGameDescriptionOut.getMinCoords().getx1());
        assertEquals(chaosGameDescription.getMaxCoords().getx0(), chaosGameDescriptionOut.getMaxCoords().getx0());
        assertEquals(chaosGameDescription.getMaxCoords().getx1(), chaosGameDescriptionOut.getMaxCoords().getx1());
        assertEquals(chaosGameDescription.getTransforms().size(), chaosGameDescriptionOut.getTransforms().size());
    }
}