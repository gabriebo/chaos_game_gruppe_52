package edu.ntnu.stud.chaosgame.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JuliaTransformTest {

  @Test
  void testGetPoint() {
    Complex complex = new Complex(1.0, 2.0);
    JuliaTransform juliaTransform = new JuliaTransform(complex, 1);

    Complex result = juliaTransform.getPoint();

    assertEquals(complex, result);
  }

  @Test
  void testTransformWithPositiveSign() {
    Complex c = new Complex(1.0, 2.0);
    JuliaTransform juliaTransform = new JuliaTransform(c, 1);
    Vector2D inputPoint = new Vector2D(3.0, 4.0);

    Vector2D result = juliaTransform.transform(inputPoint);

    // Expected result for sqrt((2.0, 2.0)):
    double expectedReal = 1.5537739740300374; // calculated by ChatGPT
    double expectedImaginary = 0.6435942529055826; // calculated by ChatGPT

    assertEquals(expectedReal, result.getx0(), 1e-10);
    assertEquals(expectedImaginary, result.getx1(), 1e-10);
  }

  @Test
  void testTransformWithNegativeSign() {
    Complex c = new Complex(1.0, 2.0);
    JuliaTransform juliaTransform = new JuliaTransform(c, -1);
    Vector2D inputPoint = new Vector2D(3.0, 4.0);

    Vector2D result = juliaTransform.transform(inputPoint);

    // Expected result for -sqrt((2.0, 2.0)):
    double expectedReal = -1.5537739740300374; // manually calculated or from a reliable source
    double expectedImaginary = -0.6435942529055826; // manually calculated or from a reliable source

    assertEquals(expectedReal, result.getx0(), 1e-10); // Adjust precision as needed
    assertEquals(expectedImaginary, result.getx1(), 1e-10); // Adjust precision as needed
  }

  @Test
  void testTransformWithNullPoint() {
    Complex complex = new Complex(1.0, 2.0);
    JuliaTransform juliaTransform = new JuliaTransform(complex, 1);

    assertThrows(IllegalArgumentException.class, () -> juliaTransform.transform(null));
  }

  @Test
  void testTransformWithInvalidSign() {
    Complex complex = new Complex(1.0, 2.0);
    JuliaTransform juliaTransform = new JuliaTransform(complex, 0); // Invalid sign
    Vector2D inputPoint = new Vector2D(3.0, 4.0);

    assertThrows(IllegalArgumentException.class, () -> juliaTransform.transform(inputPoint));
  }
}
