package edu.ntnu.stud.chaosgame.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class AffineTransform2DTest {

  @Test
  public void testConstructorAndGetters() {

    Matrix2x2 matrix = new Matrix2x2(1, 2, 3, 4);
    Vector2D vector = new Vector2D(5, 6);

    AffineTransform2D affineTransform = new AffineTransform2D(matrix, vector);

    assertEquals(matrix, affineTransform.getMatrix());
    assertEquals(vector, affineTransform.getVector());
  }

  @Test
  public void testTransform() {

    Matrix2x2 matrix = new Matrix2x2(2, 0, 0, 2);
    Vector2D vector = new Vector2D(1, 1);
    Vector2D point = new Vector2D(3, 4);
    AffineTransform2D affineTransform = new AffineTransform2D(matrix, vector);

    Vector2D result = affineTransform.transform(point);

    assertEquals(new Vector2D(7, 9), result);
  }

  @Test
  public void testTransformWithNonIdentityMatrix() {

    Matrix2x2 matrix = new Matrix2x2(1, 2, 3, 4);
    Vector2D vector = new Vector2D(5, 6);
    Vector2D point = new Vector2D(1, 1);
    AffineTransform2D affineTransform = new AffineTransform2D(matrix, vector);

    Vector2D result = affineTransform.transform(point);

    assertEquals(new Vector2D(8, 13), result);
  }

  @Test
  public void testTransformWithZeroMatrix() {

    Matrix2x2 matrix = new Matrix2x2(0, 0, 0, 0);
    Vector2D vector = new Vector2D(5, 6);
    Vector2D point = new Vector2D(3, 4);
    AffineTransform2D affineTransform = new AffineTransform2D(matrix, vector);

    Vector2D result = affineTransform.transform(point);

    assertEquals(new Vector2D(5, 6), result);
  }

  @Test
  public void testTransformWithNegativeValues() {

    Matrix2x2 matrix = new Matrix2x2(-1, 0, 0, -1);
    Vector2D vector = new Vector2D(1, 1);
    Vector2D point = new Vector2D(3, 4);
    AffineTransform2D affineTransform = new AffineTransform2D(matrix, vector);

    Vector2D result = affineTransform.transform(point);

    assertEquals(new Vector2D(-2, -3), result);
  }

  @Test
  public void testTransformWithIdentityMatrix() {

    Matrix2x2 matrix = new Matrix2x2(1, 0, 0, 1);
    Vector2D vector = new Vector2D(0, 0);
    Vector2D point = new Vector2D(3, 4);
    AffineTransform2D affineTransform = new AffineTransform2D(matrix, vector);

    Vector2D result = affineTransform.transform(point);

    assertEquals(new Vector2D(3, 4), result);
  }
}
