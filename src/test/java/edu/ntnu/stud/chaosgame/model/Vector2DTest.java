package edu.ntnu.stud.chaosgame.model;

import edu.ntnu.stud.chaosgame.model.Vector2D;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Vector2DTest {

    private Vector2D a;
    private Vector2D b;

    @BeforeEach
    void setup() {
        a = new Vector2D(1,1);
        b = new Vector2D(0,1);
    }


    @Test
    void testgetx0() {
        Assertions.assertEquals(1, a.getx0());
        Assertions.assertEquals(0, b.getx0());
    }

    @Test
    void testgetx1() {
        Assertions.assertEquals(1, a.getx1());
        Assertions.assertEquals(1, b.getx1());
    }

    @Test
    void add() {
        assertEquals(1, a.add(b).getx0());
        assertEquals(2, a.add(b).getx1());
        assertEquals(1, b.add(a).getx0());
        assertEquals(2, b.add(a).getx1());
    }

    @Test
    void subtract() {
        assertEquals(1, a.subtract(b).getx0());
        assertEquals(0, a.subtract(b).getx1());
        assertEquals(-1, b.subtract(a).getx0());
        assertEquals(0, b.subtract(a).getx1());
    }
}