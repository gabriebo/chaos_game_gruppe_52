package edu.ntnu.stud.chaosgame.model;

import javafx.scene.control.TextField;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ChaosGameDescriptionFactoryTest {
        @Test
        void testCreateSierpinskiTriangle() {

            // No arrangement needed

            ChaosGameDescription chaosGameDescription = ChaosGameDescriptionFactory.createSierpinskiTriangle();

            assertEquals(chaosGameDescription.getMinCoords().getx0(), 0);
            assertEquals(chaosGameDescription.getMinCoords().getx1(), 0);
            assertEquals(chaosGameDescription.getMaxCoords().getx0(), 1.0);
            assertEquals(chaosGameDescription.getMaxCoords().getx1(), 1.0);
            assertEquals(chaosGameDescription.getTransforms().size(), 3);
        }

        @Test
        void testCreateJuliaSet() {

            // No arrangement needed

            ChaosGameDescription chaosGameDescription = ChaosGameDescriptionFactory.createJuliaSet();

            assertEquals(chaosGameDescription.getMinCoords().getx0(), -2);
            assertEquals(chaosGameDescription.getMinCoords().getx1(), -2);
            assertEquals(chaosGameDescription.getMaxCoords().getx0(), 2);
            assertEquals(chaosGameDescription.getMaxCoords().getx1(), 2);
            assertEquals(chaosGameDescription.getTransforms().size(), 2);

        }

        @Test
        void testCreateBarnsleyFern() {

            // No arrangement needed

            ChaosGameDescription chaosGameDescription = ChaosGameDescriptionFactory.createBarnsleyFern();

            assertEquals(chaosGameDescription.getMinCoords().getx0(), -2.182);
            assertEquals(chaosGameDescription.getMinCoords().getx1(), 0.0);
            assertEquals(chaosGameDescription.getMaxCoords().getx0(), 2.6558);
            assertEquals(chaosGameDescription.getMaxCoords().getx1(), 9.9983);
            assertEquals(chaosGameDescription.getTransforms().size(), 4);
            assertEquals(chaosGameDescription.getProbabilities().size(), 4);
        }}

