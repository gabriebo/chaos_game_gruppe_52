package edu.ntnu.stud.chaosgame.model;

import edu.ntnu.stud.chaosgame.model.Matrix2x2;
import edu.ntnu.stud.chaosgame.model.Vector2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Matrix2x2Test {

    Matrix2x2 a;
    Vector2D c;
    Vector2D d;
    Vector2D e;
    Vector2D f;

    @BeforeEach
    void setUp() {
        a = new Matrix2x2(1,2,3,4);

        c = new Vector2D(1,2);
        d = new Vector2D(3,4);

        e = a.multiply(c);
        f = a.multiply(d);


    }

    @Test
    void multiply() {
        assertEquals(5, e.getx0());
        assertEquals(11, f.getx0());

        assertEquals(11, e.getx1());
        assertEquals(25, f.getx1());
    }
}