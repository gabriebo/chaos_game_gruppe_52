package edu.ntnu.stud.chaosgame.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class ChaosCanvasTest {

  private ChaosCanvas chaosCanvas;
  private Vector2D minCoords;
  private Vector2D maxCoords;

  @BeforeEach
  void setUp() {
    //Initialize the necessary objects and state
    minCoords = new Vector2D(0, 0);
    maxCoords = new Vector2D(10, 10);
    // Pass null for GraphicsContext
    chaosCanvas = new ChaosCanvas(100, 100, minCoords, maxCoords, null);
  }

  @Test
  void testSetCoords() {
    Vector2D newMinCoords = new Vector2D(-5, -5);
    Vector2D newMaxCoords = new Vector2D(5, 5);

    chaosCanvas.setCoords(newMinCoords, newMaxCoords);

    assertEquals(newMinCoords, chaosCanvas.getMinCoords());
    assertEquals(newMaxCoords, chaosCanvas.getMaxCoords());
  }

  @Test
  void testGetWidth() {
    double width = chaosCanvas.getWidth();

    assertEquals(100, width);
  }

  @Test
  void testSetWidth() {
    chaosCanvas.setWidth(200);

    assertEquals(200, chaosCanvas.getWidth());
  }

  @Test
  void testGetHeight() {
    double height = chaosCanvas.getHeight();

    assertEquals(100, height);
  }

  @Test
  void testSetHeight() {
    chaosCanvas.setHeight(200);

    assertEquals(200, chaosCanvas.getHeight());
  }

  @Test
  void testGetMinCoords() {
    Vector2D min = chaosCanvas.getMinCoords();

    assertEquals(minCoords, min);
  }

  @Test
  void testGetMaxCoords() {
    Vector2D max = chaosCanvas.getMaxCoords();

    assertEquals(maxCoords, max);
  }
}
