package edu.ntnu.stud.chaosgame.model;

import org.junit.jupiter.api.Test;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

class ChaosGameDescriptionTest {

  @Test
  void testConstructorWithProbabilities() {
    Vector2D minCoords = new Vector2D(0, 0);
    Vector2D maxCoords = new Vector2D(1, 1);
    List<Transform2D> transforms = List.of(
        new AffineTransform2D(new Matrix2x2(1, 0, 0, 1), new Vector2D(0, 0)),
        new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0.5, 0.5))
    );
    List<Double> probabilities = List.of(0.5, 0.5);

    ChaosGameDescription description = new ChaosGameDescription(minCoords, maxCoords, transforms, probabilities);

    assertEquals(minCoords, description.getMinCoords());
    assertEquals(maxCoords, description.getMaxCoords());
    assertEquals(transforms, description.getTransforms());
    assertEquals(probabilities, description.getProbabilities());
  }

  @Test
  void testConstructorWithoutProbabilities() {
    Vector2D minCoords = new Vector2D(0, 0);
    Vector2D maxCoords = new Vector2D(1, 1);
    List<Transform2D> transforms = List.of(
        new AffineTransform2D(new Matrix2x2(1, 0, 0, 1), new Vector2D(0, 0)),
        new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0.5, 0.5))
    );

    ChaosGameDescription description = new ChaosGameDescription(minCoords, maxCoords, transforms);

    assertEquals(minCoords, description.getMinCoords());
    assertEquals(maxCoords, description.getMaxCoords());
    assertEquals(transforms, description.getTransforms());
    assertNull(description.getProbabilities());
  }

  @Test
  void testGetMinCoords() {
    Vector2D minCoords = new Vector2D(0, 0);
    ChaosGameDescription description = new ChaosGameDescription(minCoords, new Vector2D(1, 1), List.of());

    Vector2D result = description.getMinCoords();

    assertEquals(minCoords, result);
  }

  @Test
  void testGetMaxCoords() {
    Vector2D maxCoords = new Vector2D(1, 1);
    ChaosGameDescription description = new ChaosGameDescription(new Vector2D(0, 0), maxCoords, List.of());

    Vector2D result = description.getMaxCoords();

    assertEquals(maxCoords, result);
  }

  @Test
  void testGetTransforms() {
    List<Transform2D> transforms = List.of(
        new AffineTransform2D(new Matrix2x2(1, 0, 0, 1), new Vector2D(0, 0)),
        new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0.5, 0.5))
    );
    ChaosGameDescription description = new ChaosGameDescription(new Vector2D(0, 0), new Vector2D(1, 1), transforms);

    List<Transform2D> result = description.getTransforms();

    assertEquals(transforms, result);
  }

  @Test
  void testGetProbabilities() {
    List<Double> probabilities = List.of(0.5, 0.5);
    ChaosGameDescription description = new ChaosGameDescription(new Vector2D(0, 0), new Vector2D(1, 1), List.of(), probabilities);

    List<Double> result = description.getProbabilities();

    assertEquals(probabilities, result);
  }
}
