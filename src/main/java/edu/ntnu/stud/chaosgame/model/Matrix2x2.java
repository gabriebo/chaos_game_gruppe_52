package edu.ntnu.stud.chaosgame.model;

/**
 * Class representing a 2x2 matrix.
 * This class provides methods for matrix operations such as multiplication with a 2D vector.
 */
public class Matrix2x2 {
  private double a00;
  private double a01;
  private double a10;
  private double a11;

  /**
   * Constructor for the Matrix2x2 class.
   *
   * @param a00 the top left element of the matrix.
   * @param a01 the top right element of the matrix.
   * @param a10 the bottom left element of the matrix.
   * @param a11 the bottom right element of the matrix.
   */
  public Matrix2x2(double a00, double a01, double a10, double a11) {
    this.a00 = a00;
    this.a01 = a01;
    this.a10 = a10;
    this.a11 = a11;
  }

  /**
   * Multiplies this matrix with a 2D vector.
   *
   * @param vector the 2D vector to multiply with.
   * @return a new Vector2D instance representing the result of the multiplication.
   */
  public Vector2D multiply(Vector2D vector) {
    double newX0 = a00 * vector.getx0() + a01 * vector.getx1();
    double newX1 = a10 * vector.getx0() + a11 * vector.getx1();

    return new Vector2D(newX0, newX1);
  }

  /**
   * Returns the top left element of the matrix.
   *
   * @return the top left element of the matrix.
   */
  public double getA00() {
    return a00;
  }

  /**
   * Returns the top right element of the matrix.
   *
   * @return the top right element of the matrix.
   */
  public double getA01() {
    return a01;
  }

  /**
   * Returns the bottom left element of the matrix.
   *
   * @return the bottom left element of the matrix.
   */
  public double getA10() {
    return a10;
  }

  /**
   * Returns the bottom right element of the matrix.
   *
   * @return the bottom right element of the matrix.
   */
  public double getA11() {
    return a11;
  }
}
