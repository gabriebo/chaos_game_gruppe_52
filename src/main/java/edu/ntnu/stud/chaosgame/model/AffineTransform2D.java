package edu.ntnu.stud.chaosgame.model;

/**
 * Class representing a 2D affine transformation.
 * An affine transformation is a combination of linear
 * transformations (rotation, scaling or shear) and a translation (or shift).
 * This class implements the Transform2D interface.
 */
public class AffineTransform2D implements Transform2D {
  private Matrix2x2 matrix;
  private Vector2D vector;

  /**
   * Constructor for the AffineTransform2D class.
   *
   * @param matrix the Matrix2x2 instance representing the linear transformation.
   * @param vector the Vector2D instance representing the translation.
   */
  public AffineTransform2D(Matrix2x2 matrix, Vector2D vector) {
    this.matrix = matrix;
    this.vector = vector;
  }

  /**
   * Returns the matrix of this affine transformation.
   *
   * @return the Matrix2x2 instance representing the linear transformation.
   */
  public Matrix2x2 getMatrix() {
    return matrix;
  }

  /**
   * Returns the vector of this affine transformation.
   *
   * @return the Vector2D instance representing the translation.
   */
  public Vector2D getVector() {
    return vector;
  }

  /**
   * Applies the affine transformation to a point.
   * This is done by multiplying the matrix with the point and then adding the vector.
   *
   * @param point the Vector2D instance representing the point to transform.
   * @return the transformed point as a Vector2D instance.
   */
  @Override
  public Vector2D transform(Vector2D point) {
    return matrix.multiply(point).add(vector);
  }
}
