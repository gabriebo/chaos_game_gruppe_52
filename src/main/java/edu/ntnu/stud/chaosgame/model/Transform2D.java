package edu.ntnu.stud.chaosgame.model;

/**
 * Interface for 2D transformations.
 * This interface provides a method for transforming a 2D vector.
 */
public interface Transform2D {
  Vector2D transform(Vector2D point);
}
