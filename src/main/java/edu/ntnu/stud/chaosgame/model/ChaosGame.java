package edu.ntnu.stud.chaosgame.model;

import java.util.List;
import java.util.Random;

/**
 * Class representing the Chaos Game.
 * The Chaos Game is a method of generating
 * fractals using a set of affine transformations.
 */
public class ChaosGame {
  private Vector2D minCoords;
  private Vector2D maxCoords;
  private List<Transform2D> transforms;
  private List<Double> probabilities;
  private Random random;
  private ChaosGameObserver observer;

  /**
   * First constructor for the ChaosGame class.
   *
   * @param minCoords  the minimum coordinates of the game area.
   * @param maxCoords  the maximum coordinates of the game area.
   * @param transforms the list of Transform2D instances
   *                   representing the affine transformations to use.
   */
  public ChaosGame(Vector2D minCoords, Vector2D maxCoords, List<Transform2D> transforms) {
    this(minCoords, maxCoords, transforms, null);
  }

  /**
   * Second constructor for the ChaosGame class.
   *
   * @param minCoords     the minimum coordinates of the game area.
   * @param maxCoords     the maximum coordinates of the game area.
   * @param transforms    the list of Transform2D instances
   *                      representing the affine transformations to use.
   * @param probabilities the list of probabilities for each transformation.
   */
  public ChaosGame(Vector2D minCoords, Vector2D maxCoords,
                   List<Transform2D> transforms, List<Double> probabilities) {
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.transforms = transforms;
    this.probabilities = probabilities;
    this.random = new Random();
  }

  /**
   * Sets the minimum coordinates of the game area.
   *
   * @param minCoords the new minimum coordinates.
   */
  public void setMinCoords(Vector2D minCoords) {
    this.minCoords = minCoords;
  }

  /**
   * Sets the maximum coordinates of the game area.
   *
   * @param maxCoords the new maximum coordinates.
   */
  public void setMaxCoords(Vector2D maxCoords) {
    this.maxCoords = maxCoords;
  }

  /**
   * Sets the list of affine transformations to use.
   *
   * @param transforms the list of Transform2D instances representing the affine transformations.
   */
  public void setTransforms(List<Transform2D> transforms) {
    this.transforms = transforms;
  }

  /**
   * Sets the list of probabilities for each transformation.
   *
   * @param probabilities the list of probabilities.
   */
  public void setProbabilities(List<Double> probabilities) {
    this.probabilities = probabilities;
  }

  /**
   * Adds an observer to the Chaos Game.
   *
   * @param observer the ChaosGameObserver instance to add.
   */
  public void addObserver(ChaosGameObserver observer) {
    this.observer = observer;
  }

  /**
   * Returns the list of transformations used by the ChaosGame.
   *
   * @return the list of Transform2D instances.
   */
  public List<Transform2D> getTransforms() {
    return transforms;
  }

  /**
   * Runs the Chaos Game for a given number of iterations.
   *
   * @param iterations the number of iterations to run the game for.
   */
  public void run(int iterations) {
    // Start at the center of the game area
    Vector2D point = new Vector2D(0.5 * (minCoords.getx0() + maxCoords.getx0()),
        0.5 * (minCoords.getx1() + maxCoords.getx1()));

    // Run the game for the given number of iterations
    for (int i = 0; i < iterations; i++) {
      // Select a random transformation and apply it to the current point
      Transform2D transform = selectTransform();
      point = transform.transform(point);

      // Notify the observer
      if (observer != null) {
        observer.update(i, point);
      }
    }
  }

  /**
   * Selects a transformation to apply based on the defined probabilities.
   *
   * @return the selected Transform2D instance.
   */
  private Transform2D selectTransform() {
    // If no probabilities are defined, select a random transformation
    if (probabilities == null || probabilities.isEmpty()) {
      return transforms.get(random.nextInt(transforms.size()));
    }

    // Select a transformation based on the probabilities
    double rand = random.nextDouble();
    double cumulativeProbability = 0.0;
    for (int i = 0; i < transforms.size(); i++) {
      cumulativeProbability += probabilities.get(i);
      if (rand <= cumulativeProbability) {
        return transforms.get(i);
      }
    }

    return transforms.get(transforms.size() - 1);  // Should not reach here
  }
}
