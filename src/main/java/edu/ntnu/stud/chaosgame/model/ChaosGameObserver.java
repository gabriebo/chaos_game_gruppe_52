package edu.ntnu.stud.chaosgame.model;

/**
 * Interface for classes that observe the ChaosGame class.
 */
public interface ChaosGameObserver {
  void update(int iteration, Vector2D point);
}