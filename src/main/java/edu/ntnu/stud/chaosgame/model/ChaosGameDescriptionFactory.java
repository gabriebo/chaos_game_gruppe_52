package edu.ntnu.stud.chaosgame.model;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;


/**
 * Factory class for creating instances of ChaosGameDescription.
 * This class provides methods for creating ChaosGameDescription
 * instances for specific fractals, as well as from user input.
 */
public class ChaosGameDescriptionFactory {
  /**
   * Factory class for creating instances of ChaosGameDescription.
   * This class provides methods for creating ChaosGameDescription
   * instances for specific fractals, as well as from user input.
   */
  public static ChaosGameDescription createSierpinskiTriangle() {
    // Set the minimum and maximum coordinates of the game area
    Vector2D minCoords = new Vector2D(0, 0);
    Vector2D maxCoords = new Vector2D(1, 1);
    // Create a list of transformations
    List<Transform2D> transforms = List.of(
        new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0, 0)),
        new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0.25, 0.5)),
        new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0.5, 0))
    );
    return new ChaosGameDescription(minCoords, maxCoords, transforms);
  }

  /**
   * Creates a ChaosGameDescription for the Barnsley Fern fractal.
   *
   * @return a ChaosGameDescription for the Barnsley Fern fractal.
   */
  public static ChaosGameDescription createBarnsleyFern() {
    // Set the minimum and maximum coordinates of the game area
    Vector2D minCoords = new Vector2D(-2.182, 0);
    Vector2D maxCoords = new Vector2D(2.6558, 9.9983);
    // Create a list of transformations
    List<Transform2D> transforms = List.of(
        new AffineTransform2D(new Matrix2x2(0, 0, 0, 0.16), new Vector2D(0, 0)),
        new AffineTransform2D(new Matrix2x2(0.85, 0.04, -0.04, 0.85), new Vector2D(0, 1.6)),
        new AffineTransform2D(new Matrix2x2(0.2, -0.26, 0.23, 0.22), new Vector2D(0, 1.6)),
        new AffineTransform2D(new Matrix2x2(-0.15, 0.28, 0.26, 0.24), new Vector2D(0, 0.44))
    );
    // Create a list of probabilities for each transformation
    List<Double> probabilities = List.of(0.01, 0.85, 0.07, 0.07);
    return new ChaosGameDescription(minCoords, maxCoords, transforms, probabilities);
  }

  /**
   * Creates a ChaosGameDescription for the Julia Set fractal.
   *
   * @return a ChaosGameDescription for the Julia Set fractal.
   */
  public static ChaosGameDescription createJuliaSet() {
    // Set the minimum and maximum coordinates of the game area
    Vector2D minCoords = new Vector2D(-2, -2);
    Vector2D maxCoords = new Vector2D(2, 2);
    // Create a list of transformations
    List<Transform2D> transforms = List.of(
        new JuliaTransform(new Complex(-0.74543, 0.11301), 1),
        new JuliaTransform(new Complex(-0.74543, 0.11301), -1)
    );
    return new ChaosGameDescription(minCoords, maxCoords, transforms);
  }

  /**
   * Creates a ChaosGameDescription from affine inputs.
   *
   * @param coordsInputs TextFields containing the min and max coordinates.
   * @param affineInputs TextFields containing the affine transformation parameters.
   * @return a ChaosGameDescription created from the affine inputs,
   *              or null if the inputs are invalid.
   */
  public static ChaosGameDescription createFromAffineInputs(TextField[] coordsInputs,
                                                            TextField[][] affineInputs) {
    // Get the minimum and maximum coordinates from the input fields
    Vector2D minCoords = getCoords(coordsInputs, 0, 1);
    Vector2D maxCoords = getCoords(coordsInputs, 2, 3);

    // Show an alert if the coordinates are invalid
    if (minCoords == null || maxCoords == null) {
      showAlert("Invalid input for coords values. Please enter valid numbers.");
      return null;
    }

    // Get the list of affine transformations from the input fields
    List<Transform2D> transforms = getTransforms(affineInputs);

    // Show an alert if no transformations were created
    if (transforms.isEmpty()) {
      showAlert("Failed to create ChaosGameDescription. "
          + "Please ensure all fields are filled correctly.");
      return null;
    }

    return new ChaosGameDescription(minCoords, maxCoords, transforms);
  }

  /**
   * Creates a ChaosGameDescription from Julia inputs.
   *
   * @param juliaCoordsInputs TextFields containing the min and max coordinates.
   * @param juliaInputs TextFields containing the Julia Set parameters.
   * @return a ChaosGameDescription created from the Julia inputs,
   *        or null if the inputs are invalid.
   */
  public static ChaosGameDescription createFromJuliaInputs(TextField[] juliaCoordsInputs,
                                                           TextField[] juliaInputs) {
    // Get the minimum and maximum coordinates from the input fields
    Vector2D minCoords = getCoords(juliaCoordsInputs, 0, 1);
    Vector2D maxCoords = getCoords(juliaCoordsInputs, 2, 3);

    // Show an alert if the coordinates are invalid
    if (minCoords == null || maxCoords == null) {
      showAlert("Invalid input for coords values. Please enter valid numbers.");
      return null;
    }

    // Get the Julia Set parameters from the input fields
    List<Transform2D> transforms = new ArrayList<>();
    // Add two JuliaTransforms with positive and negative signs
    try {
      double realPart = Double.parseDouble(juliaInputs[0].getText());
      double imaginaryPart = Double.parseDouble(juliaInputs[1].getText());
      transforms.add(new JuliaTransform(new Complex(realPart, imaginaryPart), 1));
      transforms.add(new JuliaTransform(new Complex(realPart, imaginaryPart), -1));
    } catch (NumberFormatException e) { // Show an alert if the input is invalid
      showAlert("Invalid input for Julia values. Please enter valid numbers.");
      return null;
    }

    return new ChaosGameDescription(minCoords, maxCoords, transforms);
  }

  /**
   * Helper method to get coordinates from TextFields.
   *
   * @param coordsInputs TextFields containing the coordinates.
   * @param indexOfX Index of the TextField containing the x-coordinate.
   * @param indexOfY Index of the TextField containing the y-coordinate.
   * @return a Vector2D containing the coordinates, or null if the inputs are invalid.
   */
  private static Vector2D getCoords(TextField[] coordsInputs, int indexOfX, int indexOfY) {
    try {
      // Parse the x and y coordinates from the input fields
      double x = Double.parseDouble(coordsInputs[indexOfX].getText());
      double y = Double.parseDouble(coordsInputs[indexOfY].getText());
      // Return the coordinates as a Vector2D instance
      return new Vector2D(x, y);
      // Return null if the input is invalid
    } catch (NumberFormatException e) {
      return null;
    }
  }

  /**
   * Helper method to get a list of Transform2D instances from affine inputs.
   *
   * @param affineInputs TextFields containing the affine transformation parameters.
   * @return a list of Transform2D instances, or an empty list if the inputs are invalid.
   */
  private static List<Transform2D> getTransforms(TextField[][] affineInputs) {
    // Create a list to store the affine transformations
    List<Transform2D> transforms = new ArrayList<>();

    // Iterate over the input fields for affine transformations
    for (TextField[] inputs : affineInputs) {
      // Skip null inputs
      if (inputs == null) {
        continue;
      }

      // Check if all inputs are null
      boolean allNull = true;
      // Create arrays to store the matrix and vector values
      double[] matrix = new double[4];
      double[] vector = new double[2];

      // Iterate over the input fields
      for (int i = 0; i < inputs.length; i++) {
        // Parse the input if it is not null or empty
        if (inputs[i] != null && !inputs[i].getText().isEmpty()) {
          // Set allNull to false if at least one input is not null
          allNull = false;
          try {
            // Parse the input as a double
            if (i < 4) {
              matrix[i] = Double.parseDouble(inputs[i].getText());
            } else { // Store the values in the matrix and vector arrays
              vector[i - 4] = Double.parseDouble(inputs[i].getText());
            } // Show an alert if the input is invalid
          } catch (NumberFormatException e) {
            showAlert("Invalid input for affine values. Please enter valid numbers.");
            // Return an empty list if the input is invalid
            return new ArrayList<>();
          }
        } else {
          // Set the value to 0.0 if the input is null or empty
          if (i < 4) {
            matrix[i] = 0.0;
          } else {
            // Store the values in the matrix and vector arrays
            vector[i - 4] = 0.0;
          }
        }
      }

      if (!allNull) {
        transforms.add(new AffineTransform2D(new Matrix2x2(
            matrix[0], matrix[1], matrix[2], matrix[3]),
            new Vector2D(vector[0], vector[1])));
      }
    }

    return transforms;
  }

  // Helper method to show an alert
  private static void showAlert(String message) {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setHeaderText(null);
    alert.setTitle("Error");
    alert.setContentText(message);
    alert.showAndWait();
  }
}
