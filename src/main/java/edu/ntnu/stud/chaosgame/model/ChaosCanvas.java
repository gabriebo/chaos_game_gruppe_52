package edu.ntnu.stud.chaosgame.model;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * Class representing a canvas for drawing the Chaos Game.
 * This class implements the ChaosGameObserver interface
 * and updates the canvas whenever the ChaosGame changes.
 */
public class ChaosCanvas implements ChaosGameObserver {
  private int width;
  private int height;
  private Vector2D minCoords;
  private Vector2D maxCoords;
  private GraphicsContext gc;

  /**
   * Constructor for the ChaosCanvas class.
   *
   * @param width     the width of the canvas.
   * @param height    the height of the canvas.
   * @param minCoords the minimum coordinates of the canvas.
   * @param maxCoords the maximum coordinates of the canvas.
   * @param gc        the GraphicsContext instance used for drawing on the canvas.
   */
  public ChaosCanvas(int width, int height, Vector2D minCoords,
                     Vector2D maxCoords, GraphicsContext gc) {
    this.width = width;
    this.height = height;
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.gc = gc;
  }

  /**
   * Sets the coordinates of the canvas.
   *
   * @param minCoords the new minimum coordinates.
   * @param maxCoords the new maximum coordinates.
   */
  public void setCoords(Vector2D minCoords, Vector2D maxCoords) {
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
  }

  /**
   * Maps a value from one range to another.
   *
   * @param value the value to map.
   * @param min1  the minimum of the original range.
   * @param max1  the maximum of the original range.
   * @param min2  the minimum of the new range.
   * @param max2  the maximum of the new range.
   * @return the mapped value.
   */
  private double map(double value, double min1, double max1, double min2, double max2) {
    return min2 + (value - min1) * (max2 - min2) / (max1 - min1);
  }

  /**
   * Draws the Chaos Game on the canvas.
   *
   * @param game the ChaosGame instance to draw.
   * @param iterations the number of iterations to run the game for.
   */
  public void drawChaosGame(ChaosGame game, int iterations) {
    clear();
    game.addObserver(this);
    game.run(iterations);
  }

  /**
   * Clears the canvas.
   */
  public void clear() {
    gc.setFill(Color.BLACK);
    gc.fillRect(0, 0, width, height);
    //System.out.println("Canvas cleared");
  }

  /**
   * Returns the width of the canvas.
   *
   * @return the width of the canvas.
   */
  public double getWidth() {
    return width;
  }

  /**
   * Sets the width of the canvas.
   *
   * @param width the new width of the canvas.
   */
  public void setWidth(int width) {
    this.width = width;
  }

  /**
   * Sets the height of the canvas.
   *
   * @param height the new height of the canvas.
   */
  public void setHeight(int height) {
    this.height = height;
  }

  /**
   * Returns the height of the canvas.
   *
   * @return the height of the canvas.
   */
  public double getHeight() {
    return height;
  }

  /**
   * Returns the minimum coordinates of the canvas.
   *
   * @return the minimum coordinates of the canvas.
   */
  public Vector2D getMinCoords() {
    return minCoords;
  }

  /**
   * Returns the maximum coordinates of the canvas.
   *
   * @return the maximum coordinates of the canvas.
   */
  public Vector2D getMaxCoords() {
    return maxCoords;
  }

  @Override
  public void update(int iteration, Vector2D point) {
    double x = map(point.getx0(), minCoords.getx0(), maxCoords.getx0(), 0, width);
    // Invert y-axis for correct orientation
    double y = map(point.getx1(), minCoords.getx1(), maxCoords.getx1(), height, 0);

    // Ensure the points are within bounds
    if (x >= 0 && x < width && y >= 0 && y < height) {
      gc.setFill(Color.WHITE);
      gc.fillRect(x, y, 1, 1);
    }
  }
}
