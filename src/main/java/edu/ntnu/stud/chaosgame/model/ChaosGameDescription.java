package edu.ntnu.stud.chaosgame.model;

import java.util.List;

/**
 * Class representing a description of a Chaos Game.
 * This description includes the minimum and maximum coordinates of the game area,
 * a list of transformations, and optionally a list of probabilities
 * for each transformation.
 */
public class ChaosGameDescription {
  private Vector2D minCoords;
  private Vector2D maxCoords;
  private List<Transform2D> transforms;
  private List<Double> probabilities;

  /**
   * First constructor for the ChaosGameDescription class.
   *
   * @param minCoords the minimum coordinates of the game area.
   * @param maxCoords the maximum coordinates of the game area.
   * @param transforms the list of Transform2D instances representing the transformations to use.
   * @param probabilities the list of probabilities for each transformation.
   */
  public ChaosGameDescription(Vector2D minCoords, Vector2D maxCoords,
                              List<Transform2D> transforms, List<Double> probabilities) {
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.transforms = transforms;
    this.probabilities = probabilities;
  }

  /**
   * Second constructor for the ChaosGameDescription class.
   * This constructor does not require a list of probabilities,
   * and is used when each transformation is equally likely to be chosen.
   *
   * @param minCoords the minimum coordinates of the game area.
   * @param maxCoords the maximum coordinates of the game area.
   * @param transforms the list of Transform2D instances representing the transformations to use.
   */
  public ChaosGameDescription(Vector2D minCoords, Vector2D maxCoords,
                              List<Transform2D> transforms) {
    this(minCoords, maxCoords, transforms, null);
  }

  /**
   * Returns the minimum coordinates of the game area.
   *
   * @return the minimum coordinates as a Vector2D instance.
   */
  public Vector2D getMinCoords() {
    return minCoords;
  }

  /**
   * Returns the maximum coordinates of the game area.
   *
   * @return the maximum coordinates as a Vector2D instance.
   */
  public Vector2D getMaxCoords() {
    return maxCoords;
  }

  /**
   * Returns the list of transformations used in the game.
   *
   * @return the list of Transform2D instances.
   */
  public List<Transform2D> getTransforms() {
    return transforms;
  }

  /**
   * Returns the list of probabilities for each transformation.
   * If no probabilities were specified when the ChaosGameDescription
   * was created, this method returns null.
   *
   * @return the list of probabilities, or null if no probabilities were specified.
   */
  public List<Double> getProbabilities() {
    return probabilities;
  }
}