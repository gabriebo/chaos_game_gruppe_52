package edu.ntnu.stud.chaosgame.model;

/**
 * Class implementing the Transform2D interface for Julia transformations.
 * This class provides methods for transforming a 2D vector using a Julia transformation.
 */
public class JuliaTransform implements Transform2D {
  private Complex point;
  private int sign;

  /**
   * Constructor for the JuliaTransform class.
   *
   * @param point the complex point for the Julia transformation.
   * @param sign  the sign for the Julia transformation.
   */
  public JuliaTransform(Complex point, int sign) {
    this.point = point;
    this.sign = sign;
  }

  /**
   * Returns the complex point for the Julia transformation.
   *
   * @return the complex point for the Julia transformation.
   */
  public Complex getPoint() {
    return point;
  }

  /**
   * Transforms a 2D vector using a Julia transformation.
   *
   * @param point the 2D vector to transform.
   * @return the transformed 2D vector.
   * @throws IllegalArgumentException if the point is null or if the sign is not 1 or -1.
   */
  @Override
  public Vector2D transform(Vector2D point) {
    // Check if the point is null
    if (point == null) {
      throw new IllegalArgumentException("The point must not be null");
    }

    // Transform the point using the Julia transformation
    Complex z = new Complex(point.getx0(), point.getx1());
    Complex zc = z.subtract(this.point);
    Complex transformedPoint;

    // Check the sign of the Julia transformation
    if (sign == 1) {
      // Apply the square root function
      transformedPoint = zc.sqrt();
    } else if (sign == -1) {
      // Apply the square root function and negate the result
      transformedPoint = zc.sqrt().negate();
    } else {
      throw new IllegalArgumentException("Invalid sign: " + sign);
    }

    return new Vector2D(transformedPoint.getx0(), transformedPoint.getx1());
  }
}