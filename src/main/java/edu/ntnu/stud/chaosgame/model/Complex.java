package edu.ntnu.stud.chaosgame.model;

/**
 * Class representing a complex number, extending Vector2D.
 * This class provides methods for complex number operations
 * such as addition, subtraction, negation, and square root.
 */
public class Complex extends Vector2D {
  /**
   * Constructor for the Complex class.
   *
   * @param realPart      the real part of the complex number.
   * @param imaginaryPart the imaginary part of the complex number.
   */
  public Complex(double realPart, double imaginaryPart) {
    super(realPart, imaginaryPart);
  }

  /**
   * Calculates the square root of the complex number.
   *
   * @return a new Complex instance representing the square root of the original complex number.
   */
  public Complex sqrt() {
    double c = Math.sqrt((getx0() + Math.sqrt(getx0() * getx0() + getx1() * getx1())) / 2);
    double d = (getx1() / Math.abs(getx1())) * Math.sqrt((-getx0()
        + Math.sqrt(getx0() * getx0() + getx1() * getx1())) / 2);
    return new Complex(c, d);
  }

  /**
   * Negates the complex number.
   *
   * @return a new Complex instance representing the negation of the original complex number.
   */
  public Complex negate() {
    return new Complex(-this.getx0(), -this.getx1());
  }

  /**
   * Adds another complex number to this complex number.
   *
   * @param other the other complex number to add.
   * @return a new Complex instance representing the sum of the two complex numbers.
   */
  public Complex add(Complex other) {
    return new Complex(this.getx0() + other.getx0(), this.getx1() + other.getx1());
  }

  /**
   * Subtracts another complex number from this complex number.
   *
   * @param other the other complex number to subtract.
   * @return a new Complex instance representing the difference of the two complex numbers.
   */
  public Complex subtract(Complex other) {
    return new Complex(this.getx0() - other.getx0(), this.getx1() - other.getx1());
  }

  /**
   * Returns the real part of the complex number.
   *
   * @return the real part of the complex number.
   */
  public double getReal() {
    return getx0();
  }

  /**
   * Returns the imaginary part of the complex number.
   *
   * @return the imaginary part of the complex number.
   */
  public double getImaginary() {
    return getx1();
  }
}