package edu.ntnu.stud.chaosgame.model;

/**
 * Class representing a 2D vector.
 * This class provides methods for vector operations such as
 * addition, subtraction, and multiplication by a scalar.
 */
public class Vector2D {
  private double x0;
  private double x1;

  /**
   * Constructor for the Vector2D class.
   *
   * @param x0 the first component of the vector.
   * @param x1 the second component of the vector.
   */
  public Vector2D(double x0, double x1) {
    this.x0 = x0;
    this.x1 = x1;
  }

  /**
   * Returns the first component of the vector.
   *
   * @return the first component of the vector.
   */
  public double getx0() {
    return x0;
  }

  /**
   * Returns the second component of the vector.
   *
   * @return the second component of the vector.
   */
  public double getx1() {
    return x1;
  }

  /**
   * Adds another 2D vector to this vector.
   *
   * @param other the other 2D vector to add.
   * @return a new Vector2D instance representing the sum of the two vectors.
   */
  public Vector2D add(Vector2D other) {
    double newX0 = x0 + other.getx0();
    double newX1 = x1 + other.getx1();
    return new Vector2D(newX0, newX1);
  }

  /**
   * Subtracts another 2D vector from this vector.
   *
   * @param other the other 2D vector to subtract.
   * @return a new Vector2D instance representing the difference of the two vectors.
   */
  public Vector2D subtract(Vector2D other) {
    double newX0 = x0 - other.getx0();
    double newX1 = x1 - other.getx1();
    return new Vector2D(newX0, newX1);
  }

  /**
   * Multiplies this vector by a scalar.
   *
   * @param scalar the scalar to multiply with.
   * @return a new Vector2D instance representing
   *        the result of the multiplication.
   */
  public Vector2D multiply(double scalar) {
    double newX0 = x0 * scalar;
    double newX1 = x1 * scalar;
    return new Vector2D(newX0, newX1);
  }

  /**
   * Checks if this vector is equal to another object.
   *
   * @param obj the object to compare with.
   * @return true if the object is a Vector2D and has the same components, false otherwise.
   */
  @Override
  public boolean equals(Object obj) {
    // Check if the object is a Vector2D
    if (this == obj) {
      return true;
    }
    // Check if the object is null or of a different class
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    // Check if the components are equal
    Vector2D vector2D = (Vector2D) obj;
    return Double.compare(vector2D.x0, x0) == 0
        && Double.compare(vector2D.x1, x1) == 0;
  }
}
