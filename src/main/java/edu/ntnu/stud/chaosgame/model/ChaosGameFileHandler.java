package edu.ntnu.stud.chaosgame.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Class responsible for reading and writing ChaosGameDescription instances to and from files.
 */
public class ChaosGameFileHandler {
  /**
   * Reads a ChaosGameDescription from a file.
   *
   * @param path the path to the file.
   * @return the ChaosGameDescription read from the file.
   * @throws IOException if an I/O error occurs.
   */
  public ChaosGameDescription readFromFile(String path) throws IOException {
    // Read file
    BufferedReader reader = Files.newBufferedReader(Paths.get(path));
    String line;
    List<Transform2D> transforms = new ArrayList<>();

    // Read type of transformation
    String transformationType = reader.readLine().split("#")[0].trim();

    // Read min and max coordinates
    line = reader.readLine();
    Vector2D minCoords = parseVector2D(line);
    line = reader.readLine();
    Vector2D maxCoords = parseVector2D(line);

    // Check transformation type
    if ("Affine2D".equals(transformationType)) {
      while ((line = reader.readLine()) != null) {
        transforms.add(parseAffineTransform(line));
      }
    } else if ("Julia".equals(transformationType)) {
      while ((line = reader.readLine()) != null) {
        transforms.add(parseJuliaTransform(line, 1));
        transforms.add(parseJuliaTransform(line, -1));
      }
    } else {
      throw new IllegalArgumentException("Unknown transformation type: " + transformationType);
    }

    return new ChaosGameDescription(minCoords, maxCoords, transforms);
  }

  /**
   * Writes a ChaosGameDescription to a file.
   *
   * @param path        the path to the file.
   * @param description the ChaosGameDescription to write.
   * @throws IOException if an I/O error occurs.
   */
  public void writeToFile(String path, ChaosGameDescription description) throws IOException {
    BufferedWriter writer = Files.newBufferedWriter(Paths.get(path));

    // Write type of transformation
    String transformationType = description.getTransforms()
        .get(0) instanceof AffineTransform2D ? "Affine2D" : "Julia";
    writer.write(transformationType);
    writer.newLine();

    // Write min and max coordinates
    writer.write(description.getMinCoords().getx0() + ", " + description.getMinCoords().getx1());
    writer.newLine();
    writer.write(description.getMaxCoords().getx0() + ", " + description.getMaxCoords().getx1());
    writer.newLine();

    // Write transformations
    for (Transform2D transform : description.getTransforms()) {
      if (transform instanceof AffineTransform2D) {
        AffineTransform2D affineTransform = (AffineTransform2D) transform;
        writer.write(affineTransform.getMatrix().getA00() + ", "
            + affineTransform.getMatrix().getA01() + ", " + affineTransform.getMatrix().getA10()
            + ", " + affineTransform.getMatrix().getA11() + ", "
            + affineTransform.getVector().getx0() + ", " + affineTransform.getVector().getx1());
      } else if (transform instanceof JuliaTransform) {
        JuliaTransform juliaTransform = (JuliaTransform) transform;
        writer.write(juliaTransform.getPoint().getReal() + ", "
            + juliaTransform.getPoint().getImaginary());
      }
      writer.newLine();
    }

    writer.close();
  }

  /**
   * Parses an affine transformation from a string.
   *
   * @param line the string to parse.
   * @return the parsed affine transformation.
   */
  private Transform2D parseAffineTransform(String line) {
    line = line.split("#")[0]; // Remove comments
    String[] parts = line.split(", ");

    // Parse affine transformation parameters
    double a00 = Double.parseDouble(parts[0]);
    double a01 = Double.parseDouble(parts[1]);
    double a10 = Double.parseDouble(parts[2]);
    double a11 = Double.parseDouble(parts[3]);
    double b0 = Double.parseDouble(parts[4]);
    double b1 = Double.parseDouble(parts[5]);

    // Create affine transformation
    Matrix2x2 matrix = new Matrix2x2(a00, a01, a10, a11);
    Vector2D vector = new Vector2D(b0, b1);

    return new AffineTransform2D(matrix, vector);
  }

  /**
   * Parses a Julia transformation from a string.
   *
   * @param line the string to parse.
   * @param sign the sign to use for the transformation.
   * @return the parsed Julia transformation.
   */
  private Transform2D parseJuliaTransform(String line, int sign) {
    line = line.split("#")[0]; // Remove comments
    String[] parts = line.split(",");

    // Parse Julia transformation parameters
    double x0 = Double.parseDouble(parts[0]);
    double x1 = Double.parseDouble(parts[1]);

    // Create Julia transformation
    Complex point = new Complex(x0, x1);
    return new JuliaTransform(point, sign);
  }

  /**
   * Parses a 2D vector from a string.
   *
   * @param line the string to parse.
   * @return the parsed 2D vector.
   */
  private Vector2D parseVector2D(String line) {
    line = line.split("#")[0]; // Remove comments
    String[] parts = line.split(",");

    // Parse vector components
    double x0 = Double.parseDouble(parts[0].trim());
    double x1 = Double.parseDouble(parts[1].trim());

    return new Vector2D(x0, x1);
  }
}
