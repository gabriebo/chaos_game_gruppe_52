package edu.ntnu.stud.chaosgame;

import edu.ntnu.stud.chaosgame.view.MainView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Main class for the Chaos Game application.
 * This class extends the JavaFX Application class and overrides the start method.
 */
public class Main extends Application {

  /**
   * The main entry point for all JavaFX applications.
   * The start method is called after the init method has returned,
   * and after the system is ready for the application to begin running.
   *
   * @param primaryStage the primary stage for this application,
   *                     onto which the application scene can be set.
   * @throws Exception if an error occurs during the start of the application.
   */
  @Override
  public void start(Stage primaryStage) throws Exception {
    MainView mainView = new MainView();

    Scene scene = new Scene(mainView.getRoot(), 1200, 800);
    scene.getStylesheets().add(getClass().getResource("/styles.css").toExternalForm());

    primaryStage.setScene(scene);
    primaryStage.setTitle("Chaos Game Canvas");
    primaryStage.show();
  }

  /**
   * The main method is the entry point for all Java applications.
   *
   * @param args the command-line arguments passed to the application.
   */
  public static void main(String[] args) {
    launch(args);
  }
}
