package edu.ntnu.stud.chaosgame.view;

import edu.ntnu.stud.chaosgame.controller.ButtonController;
import edu.ntnu.stud.chaosgame.controller.LeftBoxController;
import javafx.scene.layout.VBox;

/**
 * Class representing the left box view of the application.
 * This class provides methods for creating the left box view.
 */
public class LeftBoxView {

  /**
   * Creates a new left box view with the specified button controller.
   *
   * @param buttonController the button controller for the left box view.
   * @return the created VBox representing the left box view.
   */
  public static VBox createLeftBox(ButtonController buttonController) {
    LeftBoxController leftBoxController = new LeftBoxController(buttonController);
    return leftBoxController.createLeftBox(buttonController);
  }
}
