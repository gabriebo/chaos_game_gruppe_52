package edu.ntnu.stud.chaosgame.view;

import edu.ntnu.stud.chaosgame.controller.ButtonController;
import edu.ntnu.stud.chaosgame.controller.TopBoxController;
import javafx.scene.layout.HBox;

/**
 * Class representing the top box view of the application.
 * This class provides a method for creating the top box view.
 */
public class TopBoxView {

  /**
   * Creates a new top box view with the specified button controller.
   *
   * @param buttonController the button controller for the top box view.
   * @return the created HBox representing the top box view.
   */
  public static HBox createTopBox(ButtonController buttonController) {
    TopBoxController topBoxController = new TopBoxController();
    return topBoxController.createTopBox(buttonController);
  }
}
