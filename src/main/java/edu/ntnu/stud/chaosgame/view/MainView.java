package edu.ntnu.stud.chaosgame.view;

import edu.ntnu.stud.chaosgame.controller.ButtonController;
import edu.ntnu.stud.chaosgame.controller.MinimizeController;
import edu.ntnu.stud.chaosgame.controller.ZoomController;
import edu.ntnu.stud.chaosgame.model.ChaosCanvas;
import edu.ntnu.stud.chaosgame.model.ChaosGame;
import edu.ntnu.stud.chaosgame.model.Complex;
import edu.ntnu.stud.chaosgame.model.JuliaTransform;
import edu.ntnu.stud.chaosgame.model.Transform2D;
import edu.ntnu.stud.chaosgame.model.Vector2D;
import java.util.ArrayList;
import java.util.List;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;


/**
 * Class representing the main view of the application.
 * This class provides methods for creating the main view and its components.
 */
public class MainView {

  private final Canvas canvas;
  private static VBox leftBox;
  private static HBox topBox;
  private final StackPane root;
  private final StackPane centerStackPane;
  private final BorderPane borderPane;

  /**
   * Constructor for the MainView class.
   * Initializes the main view and its components.
   */
  public MainView() {
    // Create the canvas, chaos canvas, and graphics context
    canvas = new Canvas(800, 800);
    GraphicsContext gc = canvas.getGraphicsContext2D();
    ChaosCanvas chaosCanvas = new ChaosCanvas(800, 800,
        new Vector2D(-2, -2), new Vector2D(2, 2), gc);

    // Create the chaos game with two Julia transformations
    List<Transform2D> transforms = new ArrayList<>();
    Complex complex = new Complex(-0.01, 0.01);
    JuliaTransform trans = new JuliaTransform(complex, -1);
    JuliaTransform trans2 = new JuliaTransform(complex, 1);
    transforms.add(trans);
    transforms.add(trans2);

    ChaosGame game = new ChaosGame(new Vector2D(-2, -2), new Vector2D(2, 2), transforms);

    // Create the button controller
    ButtonController buttonController = new ButtonController(chaosCanvas, gc, game);

    // Create the left box and top box
    leftBox = LeftBoxView.createLeftBox(buttonController);
    topBox = TopBoxView.createTopBox(buttonController);

    // Create the center stack pane
    centerStackPane = CenterView.createCenterPane(canvas, chaosCanvas, game);
    Label steps = new Label("Steps: " + 12500);
    steps.setPadding(new Insets(10, 20, 10, 0));


    // Create the border pane
    borderPane = new BorderPane();
    borderPane.setCenter(centerStackPane);
    // setTop(null) removes the top box
    borderPane.setTop(null);
    borderPane.setLeft(leftBox); // Set left box


    // Create the root stack pane
    root = new StackPane();
    // Add the border pane to the root stack pane
    root.getChildren().add(borderPane);
    root.getStyleClass().add("root");

    // Create the slider
    Slider slider = SliderComponent.createSlider(chaosCanvas, game, steps, buttonController);

    // Create the minimize controller and toggle button
    MinimizeController minimizeController = new MinimizeController(canvas, borderPane,
        root, centerStackPane);
    Button toggleButton = ButtonFactory.createButton("Min/Max", minimizeController::toggleView);

    // Create the zoom controller and zoom buttons
    ZoomController zoomController = new ZoomController(chaosCanvas, game, slider);
    HBox zoomButtons = zoomController.getZoomButtons();

    // Create the spacer
    HBox spacer = new HBox();
    HBox.setHgrow(spacer, Priority.ALWAYS); // Spacer takes up remaining space

    // Create the top HBox
    HBox topHbox = new HBox(toggleButton, spacer, zoomButtons);
    StackPane.setAlignment(topHbox, Pos.TOP_LEFT);
    // Add the top HBox to the center stack pane
    centerStackPane.getChildren().add(topHbox);
    topBox.getChildren().addAll(slider, steps);

    minimizeController.toggleView();

    // Bind the canvas size
    CenterView.bindCanvasSize(canvas, chaosCanvas, game, slider);
  }

  /**
   * Returns the root StackPane of the main view.
   *
   * @return the root StackPane of the main view.
   */
  public StackPane getRoot() {
    return root;
  }

  /**
   * Returns the left VBox of the main view.
   *
   * @return the left VBox of the main view.
   */
  public static VBox getLeftBox() {
    return leftBox;
  }

  /**
   * Returns the top HBox of the main view.
   *
   * @return the top HBox of the main view.
   */
  public static HBox getTopBox() {
    return topBox;
  }
}
