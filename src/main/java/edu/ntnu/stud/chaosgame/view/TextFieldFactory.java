package edu.ntnu.stud.chaosgame.view;

import javafx.scene.control.TextField;

/**
 * Factory class for creating text fields and arrays of text fields.
 */
public class TextFieldFactory {
  /**
   * Creates a new TextField with the specified text and action.
   *
   * @param text the text to display in the text field.
   * @param action the action to perform when an action on the text field is performed.
   * @return the created TextField.
   */
  public static TextField createTextField(String text, Runnable action) {
    TextField textField = new TextField(text);
    textField.setOnAction(e -> action.run());
    return textField;
  }

  /**
   * Creates a new array of TextFields with the specified size, text, and action.
   *
   * @param size the size of the text field array.
   * @param text the text to display in each text field.
   * @param action the action to perform when an action on any text field is performed.
   * @return the created array of TextFields.
   */
  public static TextField[] createTextFieldArray(int size, String text, Runnable action) {
    TextField[] textFields = new TextField[size];
    for (int i = 0; i < size; i++) {
      textFields[i] = createTextField(text, action);
    }
    return textFields;
  }

  /**
   * Creates a new 2D array of TextFields with the specified number of rows and
   * columns, text, and action.
   *
   * @param rows the number of rows in the 2D text field array.
   * @param cols the number of columns in the 2D text field array.
   * @param text the text to display in each text field.
   * @param action the action to perform when an action on any text field is performed.
   * @return the created 2D array of TextFields.
   */
  public static TextField[][] create2dTextFieldArray(int rows, int cols,
                                                     String text, Runnable action) {
    TextField[][] textFields = new TextField[rows][cols];
    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < cols; j++) {
        textFields[i][j] = createTextField(text, action);
      }
    }
    return textFields;
  }
}