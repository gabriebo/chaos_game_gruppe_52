package edu.ntnu.stud.chaosgame.view;

import edu.ntnu.stud.chaosgame.model.ChaosCanvas;
import edu.ntnu.stud.chaosgame.model.ChaosGame;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Slider;
import javafx.scene.layout.StackPane;

/**
 * Class representing the center view of the application.
 * This class provides methods for creating the center pane and binding the canvas size.
 */
public class CenterView {

  /**
   * Creates a new center pane with the specified canvas, chaos canvas, and game.
   *
   * @param canvas the canvas to add to the center pane.
   * @param chaosCanvas the chaos canvas for the game.
   * @param game the chaos game to play.
   * @return the created center pane.
   */
  public static StackPane createCenterPane(Canvas canvas, ChaosCanvas chaosCanvas, ChaosGame game) {
    StackPane centerStackPane = new StackPane();
    centerStackPane.getChildren().add(canvas);
    centerStackPane.setMinSize(800, 800);
    centerStackPane.setId("centerStackPane");
    return centerStackPane;
  }

  /**
   * Binds the size of the canvas to the chaos canvas and game,
   * and redraws the game when the size changes.
   *
   * @param canvas the canvas whose size to bind.
   * @param chaosCanvas the chaos canvas to resize and redraw.
   * @param game the chaos game to redraw.
   * @param slider the slider controlling the number of points to draw.
   */
  public static void bindCanvasSize(Canvas canvas, ChaosCanvas chaosCanvas,
                                    ChaosGame game, Slider slider) {
    // Bind the size of the canvas to the chaos canvas and redraw the game when the size changes
    canvas.widthProperty().addListener(evt -> {
      chaosCanvas.setWidth((int) canvas.getWidth());
      chaosCanvas.drawChaosGame(game, (int) slider.getValue());
    });
    canvas.heightProperty().addListener(evt -> {
      chaosCanvas.setHeight((int) canvas.getHeight());
      chaosCanvas.drawChaosGame(game, (int) slider.getValue());
    });
  }
}
