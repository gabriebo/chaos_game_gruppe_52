package edu.ntnu.stud.chaosgame.view;

import edu.ntnu.stud.chaosgame.controller.ButtonController;
import edu.ntnu.stud.chaosgame.model.ChaosCanvas;
import edu.ntnu.stud.chaosgame.model.ChaosGame;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;

/**
 * Class representing a slider component in the application.
 * This class provides a method for creating a slider.
 */
public class SliderComponent {
  /**
   * Creates a new Slider with the specified canvas, game, steps label, and button controller.
   * The slider is configured with specific properties and an action
   * to perform when its value changes.
   *
   * @param canvas           the canvas to clear and redraw when the slider value changes.
   * @param game             the chaos game to draw when the slider value changes.
   * @param steps            the label to update with the number of steps
   *                         when the slider value changes.
   * @param buttonController the button controller to update with the number of iterations
   *                         when the slider value changes.
   * @return the created Slider.
   */
  public static Slider createSlider(ChaosCanvas canvas,
                                    ChaosGame game,
                                    Label steps,
                                    ButtonController buttonController) {

    Slider slider = new Slider(0, 25000, 12500);
    slider.setShowTickLabels(true);
    slider.setShowTickMarks(true);
    slider.setMajorTickUnit(2500);
    slider.setMinorTickCount(4);
    slider.setBlockIncrement(10);
    slider.setPrefWidth(500);
    slider.setPadding(new Insets(10, 20, 10, 0));


    slider.valueProperty().addListener((observable, oldValue, newValue) -> {
      canvas.clear();
      canvas.drawChaosGame(game, newValue.intValue());
      steps.setText("Steps: " + (int) slider.getValue());
      buttonController.setIterations((int) slider.getValue());
    });
    return slider;
  }

}