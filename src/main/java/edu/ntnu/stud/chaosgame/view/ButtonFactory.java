package edu.ntnu.stud.chaosgame.view;

import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;

/**
 * Factory class for creating buttons and toggle buttons.
 */
public class ButtonFactory {

  /**
   * Creates a new Button with the specified text and action.
   *
   * @param text the text to display on the button.
   * @param action the action to perform when the button is clicked.
   * @return the created Button.
   */
  public static Button createButton(String text, Runnable action) {
    Button button = new Button(text);
    button.setOnAction(e -> action.run());
    return button;
  }

  /**
   * Creates a new ToggleButton with the specified text, group, and action.
   *
   * @param text the text to display on the toggle button.
   * @param group the ToggleGroup to which the toggle button belongs.
   * @param action the action to perform when the toggle button is clicked.
   * @return the created ToggleButton.
   */
  public static ToggleButton createToggleButton(String text, ToggleGroup group, Runnable action) {
    ToggleButton toggleButton = new ToggleButton(text);
    toggleButton.setToggleGroup(group);
    toggleButton.setOnAction(e -> action.run());
    return toggleButton;
  }
}
