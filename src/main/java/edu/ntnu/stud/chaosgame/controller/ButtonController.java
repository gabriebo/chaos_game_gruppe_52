package edu.ntnu.stud.chaosgame.controller;

import edu.ntnu.stud.chaosgame.model.ChaosCanvas;
import edu.ntnu.stud.chaosgame.model.ChaosGame;
import edu.ntnu.stud.chaosgame.model.ChaosGameDescription;
import edu.ntnu.stud.chaosgame.model.ChaosGameDescriptionFactory;
import edu.ntnu.stud.chaosgame.model.ChaosGameFileHandler;
import edu.ntnu.stud.chaosgame.model.Transform2D;
import edu.ntnu.stud.chaosgame.model.Vector2D;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;


/**
 * Controller class for handling button actions in the Chaos Game application.
 * This class is responsible for managing the ChaosCanvas, the ChaosGame model,
 * and the input fields for the Julia and Affine fractals.
 */
public class ButtonController {

  private ChaosCanvas chaosCanvas;
  private GraphicsContext gc;
  private ChaosGame game;
  private int iterations;
  private TextField[] juliaInputs;
  private TextField[][] affineInputs;
  private TextField[] coordsInputs;
  private TextField[] juliaCoordsInputs;

  /**
   * Constructor for the ButtonController class.
   *
   * @param chaosCanvas the ChaosCanvas instance used for drawing fractals.
   * @param gc the GraphicsContext instance used for drawing on the ChaosCanvas.
   * @param game the ChaosGame instance used for generating fractal descriptions.
   */
  public ButtonController(ChaosCanvas chaosCanvas, GraphicsContext gc, ChaosGame game) {
    this.chaosCanvas = chaosCanvas;
    this.gc = gc;
    this.game = game;
    // Default number of iterations
    this.iterations = 12500;
  }

  /**
   * Sets the number of iterations for the ChaosGame.
   *
   * @param iterations the number of iterations to set.
   */
  public void setIterations(int iterations) {
    // Set the number of iterations
    this.iterations = iterations;
  }

  /**
   * Handles the action for the Sierpinski Triangle button.
   * This method generates a Sierpinski Triangle fractal description
   * and draws it on the ChaosCanvas.
   */
  public void handleSierpinskiTriangle() {
    // Initialize the Sierpinski Triangle fractal description
    ChaosGameDescription description = ChaosGameDescriptionFactory.createSierpinskiTriangle();
    // Check if the description was created successfully
    if (description != null) {
      System.out.println("Sierpinski Triangle description created");
      // Set the fractal description in the ChaosGame model
      game.setMinCoords(description.getMinCoords());
      game.setMaxCoords(description.getMaxCoords());
      game.setTransforms(description.getTransforms());
      // Set the probabilities to null for the Sierpinski Triangle
      game.setProbabilities(null);
      // Set the coordinates in the ChaosCanvas
      chaosCanvas.setCoords(description.getMinCoords(), description.getMaxCoords());
      // Clear the ChaosCanvas and draw the fractal
      chaosCanvas.clear();
      chaosCanvas.drawChaosGame(game, iterations);
    }
  }

  /**
   * Handles the action for the Barnsley Fern button.
   * This method generates a Barnsley Fern fractal description and draws it on the ChaosCanvas.
   */
  public void handleBarnsleyFern() {
    // Initialize the Barnsley Fern fractal description
    ChaosGameDescription description = ChaosGameDescriptionFactory.createBarnsleyFern();
    // Check if the description was created successfully
    if (description != null) {
      System.out.println("Barnsley Fern description created");
      // Set the fractal description in the ChaosGame model
      game.setMinCoords(description.getMinCoords());
      game.setMaxCoords(description.getMaxCoords());
      game.setTransforms(description.getTransforms());
      // Set the probabilities for the Barnsley Fern
      game.setProbabilities(description.getProbabilities());
      // Set the coordinates in the ChaosCanvas
      chaosCanvas.setCoords(description.getMinCoords(), description.getMaxCoords());
      // Clear the ChaosCanvas and draw the fractal
      chaosCanvas.clear();
      chaosCanvas.drawChaosGame(game, iterations);
    }
  }

  /**
   * Handles the action for the Julia Set button.
   * This method generates a Julia Set fractal description and draws it on the ChaosCanvas.
   */
  public void handleJuliaSet() {
    // Initialize the Julia Set fractal description
    ChaosGameDescription description = ChaosGameDescriptionFactory.createJuliaSet();
    // Check if the description was created successfully
    if (description != null) {
      System.out.println("Julia Set description created");
      // Set the fractal description in the ChaosGame model
      game.setMinCoords(description.getMinCoords());
      game.setMaxCoords(description.getMaxCoords());
      game.setTransforms(description.getTransforms());
      // Set the probabilities to null for the Julia Set
      game.setProbabilities(null);
      // Set the coordinates in the ChaosCanvas
      chaosCanvas.setCoords(description.getMinCoords(), description.getMaxCoords());
      // Clear the ChaosCanvas and draw the fractal
      chaosCanvas.clear();
      chaosCanvas.drawChaosGame(game, iterations);
    }
  }

  /**
   * Handles the action for the Save Fractal button.
   * This method saves the current fractal description to a file.
   */
  public void saveFractal() {
    FileChooser fileChooser = new FileChooser();
    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(
        "TXT files (*.txt)", "*.txt"));
    File selectedFile = fileChooser.showSaveDialog(null);

    if (selectedFile != null) {
      try {
        ChaosGameFileHandler fileHandler = new ChaosGameFileHandler();
        Vector2D minCoords = chaosCanvas.getMinCoords();
        Vector2D maxCoords = chaosCanvas.getMaxCoords();
        List<Transform2D> transforms = game.getTransforms();
        ChaosGameDescription description = new ChaosGameDescription(
            minCoords, maxCoords, transforms);
        fileHandler.writeToFile(selectedFile.getAbsolutePath(), description);
      } catch (IOException ex) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setContentText("File could not be saved. Try again later.");
        alert.showAndWait();
      }
    }
  }

  /**
   * Handles the action for the Load Fractal button.
   * This method loads a fractal description from a file and draws it on the ChaosCanvas.
   */
  public void loadFractal() {
    FileChooser fileChooser = new FileChooser();
    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(
        "TXT files (*.txt)", "*.txt"));
    File selectedFile = fileChooser.showOpenDialog(null);

    if (selectedFile != null) {
      try {
        ChaosGameFileHandler fileHandler = new ChaosGameFileHandler();
        ChaosGameDescription temp = (fileHandler.readFromFile(selectedFile.getAbsolutePath()));
        game.setTransforms(temp.getTransforms());
        chaosCanvas.setCoords(temp.getMinCoords(), temp.getMaxCoords());
        chaosCanvas.drawChaosGame(game, iterations);
      } catch (NumberFormatException ex) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(null);
        alert.setContentText("Wrong format in txt.file");
        alert.showAndWait();
      } catch (IOException e) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(null);
        alert.setContentText("File could not be read. Try again later.");
        alert.showAndWait();
      }
    }
  }

  /**
   * Sets the Julia input fields.
   *
   * @param juliaInputs the array of TextField instances for the Julia inputs.
   */
  public void setJuliaInputs(TextField[] juliaInputs) {
    // Set the Julia input fields
    this.juliaInputs = juliaInputs;
  }

  /**
   * Sets the Affine input fields.
   *
   * @param affineInputs the 2D array of TextField instances for the Affine inputs.
   */
  public void setAffineInputs(TextField[][] affineInputs) {
    // Set the Affine input fields
    this.affineInputs = affineInputs;
  }

  /**
   * Sets the coordinate input fields.
   *
   * @param coordsInputs the array of TextField instances for the coordinate inputs.
   */
  public void setCoordsInputs(TextField[] coordsInputs) {
    // Set the coordinate input fields
    this.coordsInputs = coordsInputs;
  }

  /**
   * Sets the Julia coordinate input fields.
   *
   * @param juliaCoordsInputs the array of TextField instances for the Julia coordinate inputs.
   */
  public void setJuliaCoordsInputs(TextField[] juliaCoordsInputs) {
    // Set the Julia coordinate input fields
    this.juliaCoordsInputs = juliaCoordsInputs;
  }

  /**
   * Generates an Affine fractal from the current input values and draws it on the ChaosCanvas.
   */
  public void generateAffineFractal() {
    // Create a ChaosGameDescription from the Affine inputs
    ChaosGameDescription description = ChaosGameDescriptionFactory
        .createFromAffineInputs(coordsInputs, affineInputs);
    // Checks if the description was created successfully
    if (description != null) {
      // Set the fractal description in the ChaosGame model
      game.setMinCoords(description.getMinCoords());
      game.setMaxCoords(description.getMaxCoords());
      game.setTransforms(description.getTransforms());
      // Set the probabilities to null for the Affine fractal
      game.setProbabilities(null);
      // Set the coordinates in the ChaosCanvas
      chaosCanvas.setCoords(description.getMinCoords(), description.getMaxCoords());
      // Clear the ChaosCanvas and draw the fractal
      chaosCanvas.clear();
      chaosCanvas.drawChaosGame(game, iterations);
    }
  }

  /**
   * Generates a Julia fractal from the current input values and draws it on the ChaosCanvas.
   */
  public void generateJuliaFractal() {
    // Create a ChaosGameDescription from the Julia inputs
    ChaosGameDescription description = ChaosGameDescriptionFactory
        .createFromJuliaInputs(juliaCoordsInputs, juliaInputs);
    // Checks if the description was created successfully
    if (description != null) {
      // Set the fractal description in the ChaosGame model
      game.setMinCoords(description.getMinCoords());
      game.setMaxCoords(description.getMaxCoords());
      game.setTransforms(description.getTransforms());
      // Set the probabilities to null for the Julia fractal
      game.setProbabilities(null);
      // Set the coordinates in the ChaosCanvas
      chaosCanvas.setCoords(description.getMinCoords(), description.getMaxCoords());
      // Clear the ChaosCanvas and draw the fractal
      chaosCanvas.clear();
      chaosCanvas.drawChaosGame(game, iterations);
    }
  }
}
