package edu.ntnu.stud.chaosgame.controller;

import edu.ntnu.stud.chaosgame.view.ButtonFactory;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

/**
 * Controller class for handling the top box in the Chaos Game application.
 * This class is responsible for creating and managing the top box,
 * which contains buttons for predefined fractals and a quit button.
 */
public class TopBoxController {

  /**
   * Creates the top box with predefined fractal buttons and a quit button.
   *
   * @param buttonController the ButtonController instance used for handling button actions.
   * @return the created HBox instance representing the top box.
   */
  public HBox createTopBox(ButtonController buttonController) {
    HBox topBox = new HBox();
    topBox.setSpacing(10);
    topBox.setMinHeight(50); // Set minimum height for the topBox

    // Preset buttons for predefined fractals
    Button sierpinskiButton = ButtonFactory.createButton("Sierpinski Triangle",
        buttonController::handleSierpinskiTriangle);
    Button barnsleyButton = ButtonFactory.createButton("Barnsley Fern",
        buttonController::handleBarnsleyFern);
    Button juliaSetButton = ButtonFactory.createButton("Julia Set",
        buttonController::handleJuliaSet);

    Label titleLabel = new Label("Predefined Fractals");

    // Spacer
    Region spacer = new Region();
    HBox.setHgrow(spacer, Priority.ALWAYS);

    // Quit button to exit the application
    Button quitButton = ButtonFactory.createButton("Quit", () -> {
      Stage stage = (Stage) topBox.getScene().getWindow();
      stage.close();
    });

    topBox.getChildren().addAll(titleLabel, sierpinskiButton,
        barnsleyButton, juliaSetButton, spacer, quitButton);
    topBox.setId("topBox");

    return topBox;
  }
}
