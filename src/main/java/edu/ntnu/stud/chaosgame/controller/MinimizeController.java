package edu.ntnu.stud.chaosgame.controller;

import edu.ntnu.stud.chaosgame.view.MainView;
import javafx.scene.canvas.Canvas;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;

/**
 * Controller for handling the toggle between maximized and minimized views of the canvas.
 */
public class MinimizeController {

  private boolean isMaximized = true;
  private final Canvas canvas;
  private final BorderPane borderPane;
  private final StackPane root;
  private final StackPane canvasContainer;

  /**
   * Constructor for MinimizeController.
   *
   * @param canvas the canvas to be resized.
   * @param borderPane the BorderPane containing the layout.
   * @param root the root StackPane.
   * @param canvasContainer the container StackPane for the canvas.
   */
  public MinimizeController(Canvas canvas, BorderPane borderPane,
                            StackPane root, StackPane canvasContainer) {
    this.canvas = canvas;
    this.borderPane = borderPane;
    this.root = root;
    this.canvasContainer = canvasContainer;
    setMaximizedView(false); // Initialize the view to be minimized
    bindCanvasSize(); // Bind the canvas size to the initial state
  }

  /**
   * Toggles the view between maximized and minimized states.
   */
  public void toggleView() {
    isMaximized = !isMaximized; // Toggle the state
    setMaximizedView(isMaximized); // Update the view based on the new state
  }

  /**
   * Sets the view to either maximized or minimized based on the parameter.
   *
   * @param maximized if true, set the view to maximized; if false, set to minimized.
   */
  private void setMaximizedView(boolean maximized) {
    if (maximized) {
      // In maximized view, remove the top and left panels
      borderPane.setTop(null);
      borderPane.setLeft(null);
      canvasContainer.setMinSize(0, 0); // Ensure buttons remain visible
    } else {
      // In minimized view, restore the top and left panels
      borderPane.setTop(MainView.getTopBox());
      borderPane.setLeft(MainView.getLeftBox());
      canvasContainer.setMinSize(0, 0); // Ensure buttons remain visible
    }
    bindCanvasSize(); // Re-bind the canvas size to adjust to the new layout
  }

  /**
   * Binds the size of the canvas to either the root
   * or the canvas container based on the current state.
   */
  private void bindCanvasSize() {
    // Unbind any previous size bindings
    canvas.widthProperty().unbind();
    canvas.heightProperty().unbind();

    if (isMaximized) {
      // Bind the canvas size to the root size when maximized
      canvas.widthProperty().bind(root.widthProperty());
      canvas.heightProperty().bind(root.heightProperty());
    } else {
      // Bind the canvas size to the canvas container size when minimized
      canvas.widthProperty().bind(canvasContainer.widthProperty());
      canvas.heightProperty().bind(canvasContainer.heightProperty());
    }
  }
}
