package edu.ntnu.stud.chaosgame.controller;

import edu.ntnu.stud.chaosgame.model.ChaosCanvas;
import edu.ntnu.stud.chaosgame.model.ChaosGame;
import edu.ntnu.stud.chaosgame.model.ChaosGameObserver;
import edu.ntnu.stud.chaosgame.model.Vector2D;
import edu.ntnu.stud.chaosgame.view.ButtonFactory;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;

/**
 * Controller class for handling zoom functionality in the Chaos Game application.
 * This class is responsible for creating and managing the zoom buttons and slider,
 * and updating the ChaosCanvas when zooming.
 */
public class ZoomController implements ChaosGameObserver {
  private final double zoomStep = 1.1; // Factor to multiply/divide for zooming
  private final ChaosCanvas chaosCanvas;
  private final ChaosGame chaosGame;
  private final HBox zoomButtons;
  private Slider slider;

  /**
   * Controller class for handling zoom functionality in the Chaos Game application.
   * This class is responsible for creating and managing the zoom buttons and slider,
   * and updating the ChaosCanvas when zooming.
   */
  public ZoomController(ChaosCanvas chaosCanvas, ChaosGame chaosGame, Slider slider) {
    this.chaosCanvas = chaosCanvas;
    this.chaosGame = chaosGame;
    this.zoomButtons = createZoomButtons();
    this.slider = slider;
  }

  /**
   * Creates the zoom buttons.
   *
   * @return the created HBox instance containing the zoom buttons.
   */
  private HBox createZoomButtons() {
    Button zoomInButton = ButtonFactory.createButton("Zoom In",
        () -> zoom(true)); // true for zoom in
    Button zoomOutButton = ButtonFactory.createButton("Zoom Out",
        () -> zoom(false)); // false for zoom out

    // Create an HBox to contain the zoom buttons
    HBox zoomButtons = new HBox(5, zoomOutButton, zoomInButton);
    // Align the zoom buttons to the top right corner
    StackPane.setAlignment(zoomButtons, Pos.TOP_RIGHT);
    return zoomButtons;
  }

  /**
   * Zooms in or out based on the given parameter.
   *
   * @param zoomIn true to zoom in, false to zoom out.
   */
  private void zoom(boolean zoomIn) {
    adjustCanvasCoordinates(zoomIn ? 1 / zoomStep : zoomStep); // Checks if zoomIn is true
    update(0, null);  // Notify update manually
  }

  /**
   * Adjusts the coordinates of the ChaosCanvas based on the given zoom factor.
   *
   * @param factor the zoom factor to apply.
   */
  private void adjustCanvasCoordinates(double factor) {
    // Get the current coordinates of the canvas
    Vector2D minCoords = chaosCanvas.getMinCoords();
    Vector2D maxCoords = chaosCanvas.getMaxCoords();

    // Calculate the new center, width, and height based on the zoom factor
    double centerX = (minCoords.getx0() + maxCoords.getx0()) / 2;
    double centerY = (minCoords.getx1() + maxCoords.getx1()) / 2;
    double width = (maxCoords.getx0() - minCoords.getx0()) * factor;
    double height = (maxCoords.getx1() - minCoords.getx1()) * factor;

    // Set the new coordinates for the canvas
    chaosCanvas.setCoords(new Vector2D(centerX - width / 2, centerY - height / 2),
        new Vector2D(centerX + width / 2, centerY + height / 2));
  }

  /**
   * Returns the HBox instance containing the zoom buttons.
   *
   * @return the HBox instance containing the zoom buttons.
   */
  public HBox getZoomButtons() {
    return zoomButtons;
  }

  /**
   * Updates the ChaosCanvas by clearing it
   * and drawing the ChaosGame with the current number of iterations.
   *
   * @param iteration the current iteration (not used).
   * @param point the current point (not used).
   */
  @Override
  public void update(int iteration, Vector2D point) {
    chaosCanvas.clear();
    chaosCanvas.drawChaosGame(chaosGame, (int) slider.getValue());
  }
}
