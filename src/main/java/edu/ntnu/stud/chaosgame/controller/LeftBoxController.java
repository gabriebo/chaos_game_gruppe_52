package edu.ntnu.stud.chaosgame.controller;

import edu.ntnu.stud.chaosgame.view.ButtonFactory;
import edu.ntnu.stud.chaosgame.view.TextFieldFactory;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;


/**
 * This class is responsible for controlling the left box of the UI.
 * It manages the creation and functionality of various
 * UI components such as buttons and text fields.
 */
public class LeftBoxController {

  // UI components that will be added to the left box
  private GridPane inputGrid;
  private int currentTransformationIndex = 0;
  private final TextField[][] transformInputs = TextFieldFactory.create2dTextFieldArray(5, 6, "",
      () -> {}); // no-op action
  private final TextField[] juliaInputs = TextFieldFactory.createTextFieldArray(2, "", () -> {});
  private final TextField[] coordsInputs = TextFieldFactory.createTextFieldArray(4, "", () -> {});
  private final TextField[] juCoordInputs = TextFieldFactory.createTextFieldArray(4, "", () -> {});
  private VBox transformationsContainer;
  private HBox toggleButtonsBox;
  private Button submitAffineButton;
  private Button submitJuliaButton;
  private ButtonController buttonController;

  /**
   * Constructor for the LeftBoxController class.
   * Initializes the buttonController and passes the necessary input fields to it.
   *
   * @param buttonController the controller for handling button actions.
   */
  public LeftBoxController(ButtonController buttonController) {
    // Assign the button controller
    this.buttonController = buttonController;

    // Pass the TextFields to the ButtonController
    this.buttonController.setJuliaInputs(juliaInputs); // Pass Julia inputs to ButtonController
    this.buttonController.setAffineInputs(transformInputs); // Pass affine input to ButtonController
    this.buttonController.setCoordsInputs(coordsInputs); // Pass coords inputs to ButtonController
    this.buttonController.setJuliaCoordsInputs(juCoordInputs); // Pass Julia coords
    // inputs to ButtonController

    // Initialize submit buttons. The buttons' actions are set in the ButtonController.
    submitJuliaButton = ButtonFactory.createButton("Submit Julia Values",
        buttonController::generateJuliaFractal);
    submitAffineButton = ButtonFactory.createButton("Submit Affine Values",
        buttonController::generateAffineFractal);
  }

  /**
   * Creates the left box UI component with all buttons and input fields.
   *
   * @param buttonController the controller for handling button actions.
   * @return a VBox containing the left box UI components.
   *      This is later added to the main UI layout.
   */
  public VBox createLeftBox(ButtonController buttonController) {
    Button saveFractalButton = ButtonFactory.createButton("Save Fractal",
        buttonController::saveFractal);
    Button loadFractalButton = ButtonFactory.createButton("Load Fractal",
        buttonController::loadFractal);

    // Create an HBox for the file buttons. These buttons are used for saving and loading fractals.
    HBox fileButtonsBox = new HBox(10, saveFractalButton, loadFractalButton);
    fileButtonsBox.setSpacing(10);

    // Initialize the input grid for transformation inputs
    inputGrid = new GridPane();
    inputGrid.setHgap(10);
    inputGrid.setVgap(10);

    // Buttons for selecting fractal type.
    // These buttons will switch between affine and Julia fractals.
    Button affineButton = ButtonFactory.createButton("Affine Fractal", this::showAffineInputs);
    Button juliaButton = ButtonFactory.createButton("Julia Fractal", this::showJuliaInputs);

    // HBox for fractal type selection buttons
    HBox fractalTypeButtonsBox = new HBox(10, affineButton, juliaButton);

    // Toggle buttons for selecting transformation
    toggleButtonsBox = createToggleButtons();

    // Label for transformation section
    Label transformationLabel = new Label("Transformations");

    // VBox to hold the transformation label and toggle buttons
    transformationsContainer = new VBox(5, transformationLabel, toggleButtonsBox);

    // VBox to hold the fractal type selection and input fields
    // The order of components in this VBox determines the order in which they are displayed
    VBox selectorAndInputsBox = new VBox(
        fileButtonsBox,
        fractalTypeButtonsBox,
        transformationsContainer,
        inputGrid
    );

    selectorAndInputsBox.setSpacing(10);
    selectorAndInputsBox.setId("selectorAndInputsBox");
    VBox.setVgrow(selectorAndInputsBox, Priority.ALWAYS);

    // VBox to hold all components in the left box
    VBox contentBox = new VBox(
        selectorAndInputsBox
    );

    contentBox.setSpacing(10);
    contentBox.setAlignment(Pos.TOP_LEFT);
    VBox.setVgrow(contentBox, Priority.ALWAYS);

    VBox leftBox = new VBox(
        contentBox
    );

    leftBox.setSpacing(10);
    leftBox.setMinWidth(150); // Set minimum width for the leftBox
    VBox.setVgrow(leftBox, Priority.ALWAYS); // Ensure leftBox grows with the layout
    leftBox.setId("leftBox");

    // Show affine inputs by default
    // The fire() method simulates a button click
    affineButton.fire();

    return leftBox;
  }

  /**
   * Creates toggle buttons for selecting the current transformation. Loop generated by ChatGPT.
   *
   * @return the HBox containing the toggle buttons.
   */
  private HBox createToggleButtons() {
    // Create an HBox to hold the toggle buttons
    HBox toggleButtonsBox = new HBox(5);
    ToggleGroup toggleGroup = new ToggleGroup();
    ToggleButton[] toggleButtons = new ToggleButton[5];

    // Create toggle buttons for each transformation.
    for (int i = 0; i < 5; i++) {
      int index = i;
      // Create a toggle button with the text "T1", "T2", etc.
      toggleButtons[i] = ButtonFactory.createToggleButton("T" + (i + 1), toggleGroup,
          () -> switchTransformation(index));
      // Add the toggle button to the HBox
      toggleButtonsBox.getChildren().add(toggleButtons[i]);
    }

    // Initially select the first toggle button
    toggleButtons[0].setSelected(true);

    return toggleButtonsBox;
  }

  /**
   * Switches the currently selected transformation and updates the input fields accordingly.
   *
   * @param index the index of the transformation to switch to.
   */
  private void switchTransformation(int index) {
    // Simply update the current transformation index and show the corresponding input fields
    currentTransformationIndex = index;
    showTransformationInputs(index);
  }

  /**
   * Displays input fields for a specific affine transformation.
   * It clears the inputGrid,
   * adds coordinate and transformation matrix fields, and the submit button.
   *
   * @param index the index of the affine transformation to display.
   */
  private void showTransformationInputs(int index) {
    inputGrid.getChildren().clear();

    // Adds coordinate input fields for affine fractal
    addInputField("X min", coordsInputs[0], -2, 0);
    addInputField("Y min", coordsInputs[1], -2, 1);
    addInputField("X max", coordsInputs[2], -2, 2);
    addInputField("Y max", coordsInputs[3], -2, 3);

    // Add transformation input fields
    String[] labels = {
        "Matrix a00:", "Matrix a01:", "Matrix a10:", "Matrix a11:", "b0:", "b1:"
    };

    // Add input fields for affine transformation
    for (int i = 0; i < labels.length; i++) {
      addInputField(labels[i], transformInputs[index][i], index, i + 4);
    }

    // Add the submit button for affine inputs
    inputGrid.add(submitAffineButton, 0, labels.length + 4, 2, 1); // Place after all inputs
  }

  /**
   * Prepares the UI for displaying affine transformations.
   * It makes transformationsContainer visible, clears the inputGrid,
   * and calls showTransformationInputs.
   * Triggered when the Affine fractal button is clicked.
   */
  private void showAffineInputs() {
    // Show the transformation title and buttons
    transformationsContainer.setVisible(true);
    // Clear the input grid
    inputGrid.getChildren().clear();
    // Show the input fields for the current transformation
    showTransformationInputs(currentTransformationIndex);
  }

  /**
   * Displays the input fields for Julia set parameters.
   * Triggered when the Julia fractal button is clicked.
   */
  private void showJuliaInputs() {
    // Hide the transformation title and buttons
    transformationsContainer.setVisible(false);
    // Clear the input grid
    inputGrid.getChildren().clear();

    // Adds coordinate input fields for Julia fractal
    addInputField("X min", juCoordInputs[0], -3, 0);
    addInputField("Y min", juCoordInputs[1], -3, 1);
    addInputField("X max", juCoordInputs[2], -3, 2);
    addInputField("Y max", juCoordInputs[3], -3, 3);

    String[] labels = {"Real Part of c:", "Imaginary Part of c:"};

    // Add input fields for Julia set parameters
    for (int i = 0; i < labels.length; i++) {
      addInputField(labels[i], juliaInputs[i], -1, i + 4);
    }

    // Add the submit button for Julia inputs
    inputGrid.add(submitJuliaButton, 0, labels.length + 5, 2, 1); // Place after all inputs
  }

  /**
   * Adds an input field to the input grid. Generated by ChatGPT.
   *
   * @param labelText     the label text for the input field.
   * @param textField     the TextField for the input.
   * @param transformIndex the index of the transformation (use -1 for Julia inputs,
   *                       -2 for coords inputs, -3 for Julia coords inputs).
   * @param inputIndex    the index of the input field.
   */
  private void addInputField(String labelText, TextField textField,
                             int transformIndex, int inputIndex) {
    Label label = new Label(labelText);
    if (textField == null) {
      textField = TextFieldFactory.createTextField("", () -> {
        // Logic to handle input value change
      });
      if (transformIndex >= 0) {
        transformInputs[transformIndex][inputIndex - 4] = textField; // Adjust for offset
      } else if (transformIndex == -1) {
        juliaInputs[inputIndex - 4] = textField; // Adjust for offset
      } else if (transformIndex == -3) {
        juCoordInputs[inputIndex] = textField;
      } else {
        coordsInputs[inputIndex] = textField;
      }
    }

    inputGrid.add(label, 0, inputIndex); // Add the label to the grid
    inputGrid.add(textField, 1, inputIndex); // Add the text field to the grid
  }
}
