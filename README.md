# ChaosGame

## Description
This is an application for visualizing of fractals. Fractals can be made by playing a chaosgame through inputting variable for affine transformations or julia transformations. There is also a set of pre-defined fractals the user may choose to see.

## Test
To ensure that all the tests are running properly type in the command "mvn test" or "mvn clean package" after moving to the project folder.

## Installation
To run the application, make sure to first have java and javafx installed on your computer.
The application can be run in command line after moving inside the project folder and running the command "mvn javafx:run".
